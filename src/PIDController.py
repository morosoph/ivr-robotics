#! /usr/bin/env python
# credits:
# http://www.inpharmix.com/jps/PID_Controller_For_Lego_Mindstorms_Robots.html

from __future__ import print_function

import ev3dev.ev3 as ev3
import robot


class PIDController():

    def __init__(self, Tp=30, Kp=0.35, Ki=0.10, Kd=0.20, killThresh=75, killTimes = 80):
        self.Tp = Tp
        self.Ki = Ki
        self.Kd = Kd
        self.killThresh = killThresh
        self.killTimes = self.killTimes

        self.whiteValue = 90
        self.blackValue = 8
        self.offset = (self.whiteValue + self.blackValue)/2

        if Kp == -1:
            self.Kp = self.Tp/self.offset
        else:
            self.Kp = Kp

    def checkPower(self, turn):
        powerValue = self.Tp + turn
        if powerValue > 100:
            return 100
        if powerValue < -100:
            return -100
        return powerValue

    def getAverageColorReading(self):
        # inputReady = raw_input("Press any key when ready:")
        tempValue = 0
        # Taking 20 values for colour reading consistency
        for i in range(20):
            tempValue += robot.color.value()
        return int(tempValue/20)

    def calibrateColors(self):
        print("Keep the with colour sensor facing white")
        self.whiteValue = self.getAverageColorReading()
        print("Keep the with colour sensor facing black")
        self.blackValue = self.getAverageColorReading()
        print("Colours calibrated. White Value: {} Black Value:{}".format(
            self.whiteValue, self.blackValue))

    # TODO: Calculate the min and max values when intialising
    def startRun(self, switchMotors=False, killPID=False):
        robot.color.mode = 'COL-REFLECT'
        robot.checkStatus()

        print("Error ranges from -{} to {}".format(self.offset, self.offset))

        lastError = 0
        integral = 0
        derivative = 0

        while True and not killPID:
            self.sv = robot.sonar.value()
            if self.killTimes == 0:
                break
            if robot.color.value() > self.killThresh:
                self.killTimes -= 1
            else:
                self.killTimes = 80  # Reset
            error = robot.color.value() - self.offset
            integral = (2/3) * integral + error
            derivative = error - lastError
            lastError = error
            turn = (self.Kp * error) +\
                   (self.Ki * integral) +\
                   (self.Kd * derivative)

            powerA = self.checkPower(turn)
            powerB = self.checkPower(turn*(-1))
            if switchMotors:
                robot.leftMotor.run_timed(duty_cycle_sp=powerB, time_sp=500)
                robot.rightMotor.run_timed(duty_cycle_sp=powerA, time_sp=500)
            else:
                robot.leftMotor.run_timed(duty_cycle_sp=powerA, time_sp=500)
                robot.rightMotor.run_timed(duty_cycle_sp=powerB, time_sp=500)

        print("Bitch should stop right about now...")
        ev3.Sound.speak('End of line').wait()


def runLineFollower():
    r = PIDController()
    # r.calibrateColors()
    r.startRun()


if __name__ == "__main__":
    runLineFollower()
