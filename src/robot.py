#! /usr/bin/env python

from __future__ import print_function

import ev3dev.ev3 as ev3

print("Initialising Robot...")

sonarMotor = ev3.MediumMotor('outA')
leftMotor = ev3.LargeMotor('outB')
rightMotor = ev3.LargeMotor('outC')

# We don't have to specifically define which input ports we're using
sonar = ev3.UltrasonicSensor()
gyro = ev3.GyroSensor()
color = ev3.ColorSensor()


def checkStatus():
    print("Sonar Motor Connected: {}".format(sonarMotor.connected))
    print("Left Motor Connected: {}".format(leftMotor.connected))
    print("Right Motor Connected: {}".format(rightMotor.connected))
    print("Sonar Sensor Connected: {}".format(sonar.connected))
    print("Gyro Sensor Connected: {}".format(gyro.connected))
    print("Color Sensor Connected: {}".format(color.connected))


def killMotors():
    leftMotor.stop()
    rightMotor.stop()


def runMotors(powerL, powerR):
    leftMotor.run_forever(duty_cycle_sp=powerL, time_sp=500)
    rightMotor.run_forever(duty_cycle_sp=powerR, time_sp=500)


def runMotorsTimed(powerL=50, powerR=50, timeL=1000, timeR=1000):
    leftMotor.run_timed(duty_cycle_sp=powerL, time_sp=timeL)
    rightMotor.run_timed(duty_cycle_sp=powerR, time_sp=timeR)


def gyroCorrection(angle):
    resultValue = gyro.value() + angle
    if resultValue < gyro.value():
        runMotors(-30, 30)
        print("Turning left")
    else:
        runMotors(30, -30)
        print("Turning Right")
    while True:
        currentAngle = abs(resultValue - gyro.value()) % 360
        if currentAngle < 8:
            break
    killMotors()
