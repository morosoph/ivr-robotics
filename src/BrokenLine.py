#! /usr/bin/env python

import robot
from PIDController import PIDController
import ev3dev.ev3 as ev3


def runBrokenLine(threshold=20):
    pidController = PIDController(Tp=30)
    initialAngle = robot.gyro.value()

    for i in range(3):
        if i % 2 == 0:
            if i == 0:
                pidController.startRun()
            else:
                pidController.startRun(switchMotors=True)
            turnAngle = 90 #- (robot.gyro.value() - initialAngle)
        else:
            pidController.startRun()
            turnAngle = -90 #- abs(robot.gyro.value() - initialAngle)
        ev3.Sound.speak("Finding next line").wait()
        robot.gyroCorrection(turnAngle)

        while True:
            if robot.color.value() < threshold:
                robot.killMotors()
                break
            robot.runMotors(30, 30)
        #initialAngle = robot.gyro.value()

if __name__ == "__main__":
    runBrokenLine()
