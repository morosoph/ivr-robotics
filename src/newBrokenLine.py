#! /usr/bin/env python

import robot
import ev3dev.ev3 as ev3
from PIDController import PIDController


def findLine(threshold=15):
    while robot.color.value() > 20:
        robot.runMotors(powerL=30, powerR=30)


def turnRight():
    robot.gyroCorrection(-90)
    findLine()


def turnLeft():
    robot.gyroCorrection(90)
    findLine()


def runBrokenLine():
    p = PIDController(Tp=30, killTimes=)

    for i in range(3):
        if i % 2 == 0:
            p.startRun()
            if robot.color.value() > 20:
                turnRight()
        else:
            p.startRun()
            if robot.color.value() > 20:
                turnLeft()

    robot.killMotors()
    ev3.Sound.speak('Finalized task!').wait()

if __name__ == "__main__":
    runBrokenLine()
