#! /usr/bin/env python
# credits:
# http://www.inpharmix.com/jps/PID_Controller_For_Lego_Mindstorms_Robots.html

from __future__ import print_function

import robot
import ev3dev.ev3 as ev3
from time import sleep
from PIDController import PIDController


class PIDandSonar(PIDController):

    def __init__(self, Tp=25, Kp=0.30, Ki=0.10, Kd=0.20, killThresh=75):
        self.Tp = Tp
        self.Ki = Ki
        self.Kd = Kd
        self.killThresh = killThresh

        self.whiteValue = 90
        self.blackValue = 8
        self.stopped = True
        self.offset = (self.whiteValue + self.blackValue)/2
        self.sv = robot.sonar.value()

        if Kp == -1:
            self.Kp = self.Tp/self.offset
        else:
            self.Kp = Kp

    # TODO: Calculate the min and max values when intialising
    def startRun(self, switchMotors=False, killPID=False):
        robot.color.mode = 'COL-REFLECT'
        robot.checkStatus()
        repositionSonar()

        print("Error ranges from -{} to {}".format(self.offset, self.offset))

        lastError = 0
        integral = 0
        derivative = 0
        killTimes = 20

        while True and not killPID:
            self.sv = robot.sonar.value()
            self.stopped = False

            if self.sv <= 80:
                # sev3.Sound.speak("Object Detected!").wait()
                killPID = True
                self.stopped = True
                break

            if killTimes == 0:
                break
            if robot.color.value() > self.killThresh:
                killTimes -= 1
            else:
                killTimes = 20  # Reset

            error = robot.color.value() - self.offset
            integral = (2/3) * integral + error
            derivative = error - lastError
            lastError = error
            turn = (self.Kp * error) +\
                   (self.Ki * integral) +\
                   (self.Kd * derivative)

            powerA = self.checkPower(turn)
            powerB = self.checkPower(turn*(-1))
            if switchMotors:
                robot.leftMotor.run_timed(duty_cycle_sp=powerB, time_sp=500)
                robot.rightMotor.run_timed(duty_cycle_sp=powerA, time_sp=500)
            else:
                robot.leftMotor.run_timed(duty_cycle_sp=powerA, time_sp=500)
                robot.rightMotor.run_timed(duty_cycle_sp=powerB, time_sp=500)

        # Dont delete this, its useful for testing
        print("Bitch should stop right about now...")
        # ev3.Sound.speak('I am a bitch so I won\'t work').wait()


def sonarLeftTurn():
    """
    Using absolute value, relative position can also be used
    """
    robot.sonarMotor.run_to_abs_pos(position_sp=-90, duty_cycle_sp=55)


def sonarRightTurn():
    """
    Using absolute value, relative position can also be used
    """
    robot.sonarMotor.run_to_abs_pos(position_sp=90, duty_cycle_sp=55)


def repositionSonar(timerValue=0.2):
    """
    Using absolute value, relative position can also be used
    """
    robot.sonarMotor.run_to_abs_pos(position_sp=90, duty_cycle_sp=55)
    sleep(timerValue)
    robot.sonarMotor.run_to_abs_pos(position_sp=0, duty_cycle_sp=55)
    sleep(timerValue)
    robot.sonarMotor.run_to_abs_pos(position_sp=90, duty_cycle_sp=55)
    sleep(timerValue)
    robot.sonarMotor.run_to_abs_pos(position_sp=0, duty_cycle_sp=55)
    sleep(timerValue)
    robot.sonarMotor.run_to_abs_pos(position_sp=90, duty_cycle_sp=55)
    sleep(timerValue)
    robot.sonarMotor.run_to_abs_pos(position_sp=0, duty_cycle_sp=55)


def adjustPosition(timerValue=3):
    """
    Since Ki, Kd and Kp can be finnicky to change,
    We need to position the robot so that it sits parallel to the object
    Assumes the sonar is positioned towards the object
    """
    if robot.sonar.value() > 80:
        print("Robot overshot")
        # Turn to the right
        while robot.sonar.value() > 80:
            robot.gyroCorrection(-5)
            sleep(timerValue)

    elif robot.sonar.value() < 80:
        print("Robot too close")
        # Turn to the left
        while robot.sonar.value() < 80:
            robot.gyroCorrection(5)
            sleep(timerValue)
    else:
        print("Robot is exactly where it should be!")


def evade(timerValue=0.5, evadeInstance=1):
    """
    Function that tries to move forwards
    Until object to evade not in sight anymore
    """
    print("Trying to evade object")
    print("First sonar reading is {}".format(robot.sonar.value()))

    robot.runMotorsTimed(timeL=500, timeR=500)
    sleep(timerValue)

    if evadeInstance == 2:
        for _ in range(2):
            robot.runMotorsTimed(timeL=250, timeR=250)
            sleep(timerValue)

    while robot.sonar.value() <= 200:
        print(robot.sonar.value())
        robot.runMotorsTimed(timeL=500, timeR=500)
        sleep(timerValue)

    robot.runMotorsTimed(timeL=500, timeR=500)
    sleep(timerValue)


def findLine(timerValue=0.1):
    """
    Simple function that moves forwards
    Until it finds the black line
    """
    print("Trying to find line!")

    while robot.color.value() > 15:
        robot.runMotorsTimed(timeL=100, timeR=100)
        if robot.color.value() <= 15:
            print("Found value!")
        sleep(timerValue)


def readjust(timerValue=0.1, threshold=20):
    """
    When the black line is found,
    Robot should slowly turn to the right
    Such that it end up on the left side of the line
    """
    print("Trying to readjust")
    #while True:
        #robot.runMotorsTimed(powerL=50, powerR=50, timeL=200, timeR=200)
    #ev3.Sound.speak('Found the other side of the line').wait()
    robot.gyroCorrection(180)


def objectEvasion(timerValue=0.1):
    """
    If object is found, evade it
    Uses typical 90 degree turns
    Hardcoded, but works
    """
    # Step 1: Once object is found, turn towards the left
    ev3.Sound.speak('Turning to the left!').wait()
    robot.gyroCorrection(-90)
    sleep(timerValue)

    # Step 1.5: Turn sonar towards the object
    ev3.Sound.speak('Turning sonar towards the object!').wait()
    sonarRightTurn()
    sleep(timerValue)

    # Step 1.6: Adjust position relative to object (should be parallel)
    # adjustPosition()
    sleep(timerValue)

    # Step 2: Go forwards until object not in sight anymore
    ev3.Sound.speak('Going forwards!').wait()
    evade(evadeInstance=1)
    sleep(timerValue)

    # Step 3: Once object not in sight, turn to the right
    ev3.Sound.speak('Turning to the right!').wait()
    robot.gyroCorrection(90)
    sleep(timerValue)

    # Step 4: Go forwards until object not in sight anymore
    ev3.Sound.speak('Going forwards!').wait()
    evade(evadeInstance=2)
    sleep(timerValue)

    # Step 5: Once object not in sight, turn to the right
    ev3.Sound.speak('Turning to the right!').wait()
    robot.gyroCorrection(90)
    sleep(timerValue)

    # Step 6: Turn sonar to the initial position (optional)
    ev3.Sound.speak('Repositioning sonar!').wait()
    repositionSonar()
    sleep(timerValue)

    # Step 7: Go forwards until it finds the line
    findLine()
    sleep(timerValue)
    ev3.Sound.speak('Found line!').wait()

    # Step 8 (Final): Readjust position,
    # such that it is able to reuse the PIDController
    readjust()
    sleep(timerValue)


def runObjectDetector():
    """
    Run Task C
    """
    
    while True:
        pidC = PIDandSonar(Tp=30, Kp=0.35, Ki=0.10, Kd=0.20, killThresh=75)
        pidC.startRun()
        if pidC.stopped:
            objectEvasion()



if __name__ == "__main__":
    runObjectDetector()
